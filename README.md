# Docker tools

Containers "outils" pour administration docker


**Monitoring des containers :**
*  Mise à jour auto -> **Watchtower**. Plus d'infos https://containrrr.github.io/watchtower/
*  Stats utilisation ressources -> **cAdvisor**. Plus d'infos https://github.com/google/cadvisor
*  Graph utilisation ressrouces -> **Grafana**. Plus d'infos https://github.com/grafana/grafana


Un exemple d'utilisation/tuto https://blog.eleven-labs.com/fr/monitorer-ses-containers-docker/

**Partage réseau :**
* Serveur + client SMB
* Serveur + client NFS